from simulacion import (odds, CombinacionesPorGrupo) 
from itertools import combinations
import random
## se utiliza el arreglo alumnos para hacer futuras comparaciones, este contiene los mimos elementos que el arreglo estudiante
alumnos = ["a", "b", "c", "d", "e", "f", "g", "h", "i","j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v","w", "x"]				
## grupos seleccionados de los grupos formados anteriormente 
arreglogrupos = []
arregloiteracion = []
## define la pocision del grupo en el arreglo anterior 
grupoelegido = 0
iterador = 100000
while(iterador >0):
	## lista de estudiantes disponibles para formar x grupos
	estudiantes = ["a", "b", "c", "d", "e", "f", "g", "h", "i","j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v","w", "x"]
	
	## este bucle separa a los estudiantes en 6  distintos grupos de a 4 
	
	while(len(estudiantes)!=0):
		
		## grupos formados, en cada iteracion
		arreglodinamico = []
		## ciclo que genera grupos de 4 estudiantes, disponibles  
		for c in combinations(estudiantes, 4):
			
			arreglodinamico.append(c)
			
		grupoelegido = random.choice(arreglodinamico)
		
		arreglogrupos.append(grupoelegido)

				
		## permite quitar del total de alumnos, a los alumnos previamente escogidos, para no generar repeticiones
		for i in arreglogrupos:
			for j in i:
				if( j in  estudiantes):
					
					estudiantes.remove(j)
	
	iterador = iterador -1
	## se agrega un conjunto de grupos por iteracion  
	arregloiteracion.append(arreglogrupos)		
	## se reinicia arreglo grupos para evitar repeticiones 
	arreglogrupos = []

## arreglo boleano 
## 1 = dos agrupaciones seguidas, una pareja coincide en un mismo grupo
## 0 = no hay coincidencia
RepeticionDeParejas = []

## las comparaciones se realizaran en el siguiente ciclo, tomando en cuenta una pareja arbitraria (para este caso m y l)
for i in range(len(arregloiteracion)-1):
	## marcadores, que indican si existe una coincidencia (m y l en un mismo grupo)   
	flag = 0 
	flag2 = 0
	## Se recorren los grupos generados en cada iteracion 
	for j in range(len(arregloiteracion[i])):
		
		## condicionales que permiten identificar si m, l, c y j se encuentran en un mismo grupo
		if("m" in arregloiteracion[i][j] and "l" in arregloiteracion[i][j] and "c" in arregloiteracion[i][j] and "j" in arregloiteracion[i][j]):
			flag = 1
				
		if("m" in arregloiteracion[i+1][j] and "l" in arregloiteracion[i+1][j] and "c" in arregloiteracion[i+1][j] and "j" in arregloiteracion[i+1][j]):
			flag2 = 1
	## si existe una coincidencia, entre dos iteraciones seguidas, se cumplen los requisitos del problema 		
	if(flag == 1 == flag2 ):
		RepeticionDeParejas.append(1)
	else:
		RepeticionDeParejas.append(0)
				
## variable que permitira almacenar la cantidad de coincidencias entre iteraciones totales
match = 0.00 

for i in RepeticionDeParejas:
	if (i == 1):
		match = match + 1

## odds final corresponde a la probabilidad de que un grupo completo se repita entre dos itereaciones consecutivas 
oddsfinal = (match / len(RepeticionDeParejas)) 	
print ("match", match)

## se realiza el calculo de las intersecciones que hay por alumno en los distintos grupos 
interseccionesEntreGrupos = 0
for i  in combinations(range(CombinacionesPorGrupo), 2):
	interseccionesEntreGrupos = 1 + interseccionesEntreGrupos


print("odds final" ,oddsfinal)

##“Probabilidad final” indica el cálculo obtenido entre la sustracción de la probabilidad sin dependencias 
## obtenida en odds y la cantidad de combinaciones posibles entre los grupos por la cantidad de intersecciones posibles entre los grupos
probabilidadFinal = (CombinacionesPorGrupo * odds) - (interseccionesEntreGrupos * oddsfinal) 

print ("Probabilidad final: " , probabilidadFinal)
