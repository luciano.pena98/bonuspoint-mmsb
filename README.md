# BonusPoint MMSB

Simulación estadística
El siguiente programa permite la simulacion y calculo del problema planteado, el
el cual corresponde a lo siguiente: cual es la probabilidad de que al agrupar dos 
veces 24 personas en 6 grupos de 4 individuos, al menos una pareja se repita?.

## Requerimientos 
	Sistema operativo de distribucion linux
	python3 
	libreria de python3 itertools
	pip3
## Descarga de itertools
	Si usted no presenta instalado el paquete itertools en su ordenador lo puede instalar de la siguiente manera,
	en su consola ejecute el siguiente comando: 
		
		 pip3 install more-itertools

	Esta instalacion se realiza a traves del gestor pip3, perteneciente a python, si usted tampoco cuenta con pip3, puede instalarlo
	a traves  del siguiente comando (debe ejecutar el comando como root):

		apt install python3-pip

## MODO DE EJECUCION 
	Para ejecutar el programa debe situarse a traves de la consola, dentro de la carpeta que contenga el programa, una vez dentro 
	debe ejecutar el siguiente comando:
		
		python3 simulacionDependencia.py 

	El programa se iniciara inmediatamente

## NOTAS
	- El tiempo de ejecucion del programa puede tomar entre 10 a 15 min aproximadamente.


## Autores
	- Luciano Peña
	- Matias Rojas
